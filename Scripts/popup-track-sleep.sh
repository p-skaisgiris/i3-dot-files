#!/bin/zsh

todate=$(date +%s)
cond=$(date -d 11:00 +%s)

if [ $todate -le $cond ];
then
termite -t tracksleep -e "python /home/paulius/Scripts/tracking-metrics.py" &
i3-msg 'append_layout /home/paulius/Scripts/tracksleep.json' &
xdotool click --window $(xdotool search --name "^tracksleep") left # select tracksleep
fi
