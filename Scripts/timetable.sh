#!/bin/zsh

#wal -rt

(cd /home/paulius/Scripts/cli-timetable; source .venv/bin/activate; 
python3 timetable.py)

xdotool click --window $(xdotool search --name "^Timetable terminal") 1
xdotool key --window $(xdotool search --name "^Timetable terminal") Shift+Ctrl+space # Go into escape mode in termite
xdotool key --window $(xdotool search --name "^Timetable terminal") g # Go top of terminal

