#!/bin/zsh

LVL=$(cat /sys/class/backlight/intel_backlight/brightness)

if [ $LVL -le 1000 ]
then 
    LVL=$(($LVL-250))
else 
    LVL=$(($LVL-500))    
fi

if [ $LVL -lt 0 ]
then
       LVL=0	
fi

tee /sys/class/backlight/intel_backlight/brightness <<< $LVL
