#!/bin/zsh

bg_color=#040506
text_color=#bdc9ce
htext_color=#193523

rofi -show run -lines 3 -eh 2 -width 100 -padding 450 -opacity "80" -bw 0 -color-window "$bg_color, $bg_color, $bg_color" -color-normal "$bg_color, $text_color, $bg_color, $htext_color, $text_color" -font "Monospace 18"

#rofi -show run -lines 3 -eh 2 -width 100 -padding 450 -opacity "80" -font "Monospace 18"
