#!/usr/bin/zsh

# tput sgr0 clears it
# tput bold bolds
# tput setaf 8 makes it dark grey
# tput setaf 196 makes it red
# tput setaf 15 makes it white
# tput setaf 105 makes it dim blue
# tput setaf 226 makes it yellowish

#wal -rt

#cat "/home/paulius/Personal/TODO List"

echo

#tput sgr0
#tput setaf 15
pending_updates=$(checkupdates)
if [ $pending_updates ]; then
  tput bold
  tput setaf 196
  echo "Pending updates:"
  tput sgr0
  echo $pending_updates
  echo
fi

if [ -e /home/paulius/current ]; then
  tput bold
  tput setaf 226
  cat "/home/paulius/current"
  echo
fi
