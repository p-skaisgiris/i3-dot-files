#!/bin/zsh

LVL=$(cat /sys/class/backlight/intel_backlight/brightness)

LVL=$(($LVL+500))

if [ $LVL -gt 4882 ]
then
       LVL=4882	
fi

tee /sys/class/backlight/intel_backlight/brightness <<< $LVL
