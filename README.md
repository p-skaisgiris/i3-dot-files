# Dot files for i3 window manager

Files used to make Linux window manager i3 look neat and somewhat pretty.

- Space-themed look
- Minimalistic neat polybar
- Terminal colours matching wallpaper
- Custom locking screen
- Handy scripts for displaying relevant information (pending Arch updates, TODOs, calendar, weather)

### Software used

- [compton](https://wiki.archlinux.org/index.php/Compton) - for blur effects
- [pywal](https://github.com/dylanaraps/pywal) - for matching wallpaper colours to the terminal colours
- [betterlockscreen](https://github.com/pavanjadhaw/betterlockscreen) - for an elegant locking screen
- [cli-timetable](https://gitlab.com/k-cybulski/cli-timetable) - for displaying calendar data in terminal
- [polybar](https://github.com/jaagr/polybar) - for a charming status bar
- [termite shell](https://wiki.archlinux.org/index.php/Termite) - for a minimal terminal
- [redshift](https://github.com/jonls/redshift) - for preserving eyes
- [scrot](http://freshmeat.sourceforge.net/projects/scrot) - for taking screenshots
- [dunst](https://wiki.archlinux.org/index.php/Dunst) - for displaying notifications
- [rofi](https://github.com/DaveDavenport/rofi) - for an application launcher (replacement for dmenu)

### Look inspired by

https://github.com/jaagr/dots/tree/master/.local/etc/themer/themes/space

### Preview

![Example look](./ss1.jpg)

![Example look](./ss2.jpg)

![Example look](./ss3.png)